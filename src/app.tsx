import Innertube from 'youtubei.js';
import { Client, Intents } from 'discord.js';

import { Application } from './interfaces/application';
import { OnReadyListener } from './listeners/onReadyListener';
import { OnMessageCreateListener } from './listeners/onMessageCreateListener';

const steamApplication: Application = {
  title: 'Steam',
  urlPrefix: 'steam://openurl/',
  hostname: 'steamcommunity.com',
  urlTemplate: 'https://steamcommunity.com/sharedfiles/filedetails/?id='
}

const spotifyApplication: Application = {
  title: 'Spotify',
  urlPrefix: '<spotify:/',
  hostname: 'open.spotify.com',
  urlTemplate: 'https://open.spotify.com/'
}

const youtubeApplication: Application = {
  title: 'Youtube',
  urlPrefix: '',
  hostname: 'www.youtube.com',
  urlTemplate: 'https://www.youtube.com/watch?v='
}

const token: string = (process.env['TOKEN'] as string);

const applicationCollection: Application [] = [steamApplication,
  spotifyApplication, youtubeApplication];

const discordClient = new Client({ intents: [Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILDS] });

discordClient.on('ready', () => {
  new OnReadyListener(discordClient).onReady();
});

discordClient.on('messageCreate', (message) => {
  new OnMessageCreateListener(discordClient, applicationCollection).onMessageCreate(message);
});

discordClient.login(token);
