export const logInformation = (text: string) =>
  console.log(`[${new Date().toString()}] ${text}`);
