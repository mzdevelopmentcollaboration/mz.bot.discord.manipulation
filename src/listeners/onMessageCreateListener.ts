import url from 'url';

import Innertube from 'youtubei.js';
import { Client, Message, MessageEmbed } from "discord.js"

import { Application } from '../interfaces/application';
import { onUrlMessage } from '../managers/modifyManager';
import { generateReplaceEmbed, generateYoutubeEmbed } from '../managers/embedManager';

export class OnMessageCreateListener {
  discordClient: Client;
  applicationCollection: Application[];

  constructor(discordClient: Client,
    applicationCollection: Application[]) {
    this.discordClient = discordClient;
    this.applicationCollection = applicationCollection;
  }

  onMessageCreate = (message: Message<boolean>) => {
    if (!message.author.bot) {
      const parsedContent = url.parse(message.content);
      this.applicationCollection.forEach(async application => {
        if (message.content.includes(application.urlTemplate) &&
          parsedContent.hostname != null &&
          application.hostname.match(parsedContent.hostname)) {
          let imageUrl: string = '';
          let applicationUrl: string;
          while (!imageUrl) {
            try {
              imageUrl = (message.embeds[0].thumbnail?.url as string);
              break;
            }
            catch {
              await new Promise(resolve => setTimeout(resolve, 250));
            }
          }
          let replaceMessage: boolean = true;
          let embed: MessageEmbed = new MessageEmbed();
          switch (parsedContent.hostname) {
            case 'open.spotify.com':
              applicationUrl = application.urlPrefix + parsedContent.path + '>';
              embed = generateReplaceEmbed(message.content, applicationUrl,
                imageUrl, message.author, application.title)
              break;
            case 'steamcommunity.com':
              applicationUrl = application.urlPrefix + message.content
              embed = generateReplaceEmbed(message.content, applicationUrl,
                imageUrl, message.author, application.title)
              break;
            case 'www.youtube.com':
              const innerTube = await new Innertube();
              const videoDetails = await innerTube.getDetails(
                parsedContent.query!.split('&')[0].substring(2));
              embed = generateYoutubeEmbed(message.author, videoDetails);
              replaceMessage = false;
              break;
          }
          onUrlMessage(message, embed, replaceMessage);
        }
      });
    }
  }
}
