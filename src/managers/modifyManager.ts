import { Message, MessageEmbed } from "discord.js";

import { logInformation } from "./logManager";

export const onUrlMessage = (message: Message<boolean>,
  embed: MessageEmbed, replaceMessage: boolean) => {
  try {
    if (replaceMessage) {
      message.channel.send({ embeds: [embed] });
      message.delete();
    }
    else {
      message.reply({ embeds: [embed] });
    }
    logInformation(`Modified [${message.id}] in [${message.channelId}]`);
  }
  catch (e) {
    logInformation(`There was an Issue while replacing a message [${message.id}] in [${message.channel.id}]`);
    logInformation(`The following Issue has been generated: [${e}]`);
  }
}
