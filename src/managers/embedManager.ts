import { MessageEmbed, User } from "discord.js";
import { ApiStatus } from "youtubei.js";

export const generateReplaceEmbed = (url: string, applicationUrl: string, imageUrl: string,
  messageAuthor: User, applicationTitle: string) => {
  return new MessageEmbed()
    .setColor('#0099ff')
    .setTitle(`${messageAuthor.tag}'s Message has been modified!`)
    //.setURL(url)
    .setAuthor({ name: 'Dolos', iconURL: 'https://rsc.mzdevelopment.eu/dolos/logo.png',
      url: 'https://gitlab.com/MzDevelopment' })
    .setDescription(`${messageAuthor} sent a Link to ${applicationTitle}. It has been modified!`)
    .addFields(
      { name: 'Original Link', value: url, inline: true },
      { name: 'Modified Application Link', value: applicationUrl, inline: true }
    )
    .setImage(imageUrl)
    .setTimestamp()
    .setFooter({ text: 'This Message was generated by Dolos',
      iconURL: 'https://rsc.mzdevelopment.eu/dolos/logo.png' });
}

export const generateYoutubeEmbed = (messageAuthor: User, videoDetails: any) => {
  const numberFormat = Intl.NumberFormat();
  const formattedViews = numberFormat.format(videoDetails.metadata.view_count);
  const formattedLikes = numberFormat.format(videoDetails.metadata.likes.count);
  return new MessageEmbed()
    .setColor('#0099ff')
    .setTitle('Additional Stats for this Video')
    .setAuthor({ name: 'Dolos', iconURL: 'https://rsc.mzdevelopment.eu/dolos/logo.png',
      url: 'https://gitlab.com/MzDevelopment' })
    .setDescription(`Here is some further Information for the Video sent by ${messageAuthor}`)
    .addFields(
      { name: 'Uploaded by', value: videoDetails.metadata.channel_name, inline: true },
      { name: 'Uploaded on', value: videoDetails.metadata.publish_date_text, inline: true },
      { name: 'Views', value: formattedViews, inline: true },
      { name: 'Likes', value: formattedLikes, inline: true },
      { name: 'Length', value: '' + Math.floor(videoDetails.metadata.length_seconds / 60) + ' Minutes', inline: true },
    )
    .setTimestamp()
    .setFooter({ text: 'This Message was generated by Dolos',
      iconURL: 'https://rsc.mzdevelopment.eu/dolos/logo.png' });
}
