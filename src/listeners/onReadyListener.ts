import { Client } from "discord.js";

export class OnReadyListener {
  discordClient: Client;
  
  constructor(discordClient: Client) {
    this.discordClient = discordClient;
  }

  onReady = () => {
    this.discordClient.user?.setActivity('incomming messages', { type: 'WATCHING' });
  }
}
