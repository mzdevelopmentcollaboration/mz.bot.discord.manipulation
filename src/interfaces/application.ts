export interface Application {
  title: string,
  hostname: string,
  urlPrefix: string,
  urlTemplate: string
}
